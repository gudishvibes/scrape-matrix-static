import requests
from bs4 import BeautifulSoup

ROOM = "!GibBpYxFGNraRsZOyl:matrix.org" # Techlore Main room
OFFSET = 30 # How much to increment the offsets. TODO: call it OFFSET_INCREMENT or OFFSET_STEP
INSTANCE_URL = "https://view.matrix.org/"
FILE = "" # If "", it will assume no file and print everything out
TIMEOUT = 2
PAGES = 10
PRINT_USERS = True
SEPARATOR = ""

if FILE == "": # if you want to print out
	for page in range(PAGES):
		rq_offset = OFFSET * page
		rq_url = INSTANCE_URL + "room/" + ROOM + "/"
		rq_parameters = {
			"offset": rq_offset
		}
		print(rq_url, rq_offset)
		document = requests.get(rq_url, params=rq_parameters).text
		document = BeautifulSoup(document, "html.parser")
		timeline = document.find(id="timeline").tbody
		#print(document.prettify())
		print(timeline.find_all("tr", {"class":"event"}))
		for event in timeline.find_all("tr", {"class":"event"}):
			if PRINT_USERS:
				sender = event.find("td", {"class":"sender"})
				sender = sender.text
				message = event.find("td", {"class":"message"}).text
				print(f"{sender}: {message}")
				print(SEPARATOR)
			else:
				message = event.find("td", {"class":"message"}).text
				print(message)
				print(SEPARATOR)
else:
	print("dumping to a file is not implemented. yet.")